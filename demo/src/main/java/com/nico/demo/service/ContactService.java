package com.nico.demo.service;

import com.nico.demo.converter.ContactConverter;
import com.nico.demo.dto.ContactDto;
import com.nico.demo.entidad.Contact;
import com.nico.demo.repositorio.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("contactServiceImpl")
public class ContactService {

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private ContactConverter contactConverter;


    public ContactDto addContact(ContactDto contactDto) {
        Contact contact = contactRepository.save(contactConverter.model2entity(contactDto));
        return contactConverter.entity2model(contact);
    }


    public List<ContactDto> listAllContact() {
        List<ContactDto> listaContactDto = new ArrayList<ContactDto>();
        List<Contact> listContactAux = contactRepository.findAll();
        for (Contact element : listContactAux) {
            listaContactDto.add(contactConverter.entity2model(element));
        }
        return listaContactDto;
    }


    public Contact findContactById(int id) {
        return contactRepository.findById(id);
    }


    public ContactDto findContactModelById(int id) {
        return contactConverter.entity2model(findContactById(id));
    }


    public void removeContact(int id) {
        Contact contact = findContactById(id);
        if (contact != null) {
            contactRepository.delete(contact);
        }
    }
}
