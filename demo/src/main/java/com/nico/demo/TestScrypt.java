package com.nico.demo;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class TestScrypt {

    public static void main(String[] arg){
        BCryptPasswordEncoder pe = new BCryptPasswordEncoder();
        System.out.println(pe.encode("user"));
    }
}
