package com.nico.demo.repositorio;


import com.nico.demo.entidad.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {
    Contact findById(int id);
}
