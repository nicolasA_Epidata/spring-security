package com.nico.demo.controller;

import com.nico.demo.constant.ViewConstant;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoguinController {

    public static final Log LOG = LogFactory.getLog(LoguinController.class);

    @GetMapping("/login")
    public String showLoguinForm(Model model,
                                 @RequestParam(name = "error", required = false) String error,
                                 @RequestParam(name = "logout", required = false) String logout) {
        LOG.info("METHOD: showLoguinForm()" + "--PARAM: " + error + " " + logout);
        model.addAttribute("error", error);
        model.addAttribute("logout", logout);
        LOG.info("Returning to login view");
        return ViewConstant.LOGIN_VIEW;
    }

    @GetMapping({"/loginsuccess", "/"})
    public String loguinCheck() {
        LOG.info("METHOD: loguinCheck() ");
        LOG.info("Returning to contact view");
        return "redirect:/contacts/showcontacts";

    }
}
