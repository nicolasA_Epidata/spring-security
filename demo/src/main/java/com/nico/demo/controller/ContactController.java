package com.nico.demo.controller;

import com.nico.demo.constant.ViewConstant;
import com.nico.demo.dto.ContactDto;
import com.nico.demo.service.ContactService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/contacts")
public class ContactController {

    public static final Log LOG = LogFactory.getLog(ContactController.class);

    @Autowired
    private ContactService contactService;

    @GetMapping("/cancel")
    public String cancel() {
        return ViewConstant.CONTACTS_VIEW;
    }

    @GetMapping("/contactform")
    public String redirectContactForm(@RequestParam(name = "id", required = false) int id,
                                      Model model) {
        ContactDto contact = new ContactDto();
        if (id != 0) {
            contact = contactService.findContactModelById(id);
        }
        model.addAttribute("contactDto", contact);
        return ViewConstant.CONTACT_FORM_VIEW;
    }

    @PostMapping("/addcontact")
    public String addContact(@ModelAttribute(name = "contactDto") ContactDto contactDto, Model model) {
        LOG.info("METHOD: addContact() " + "--PARAM: " + contactDto.toString());
        if (null != contactService.addContact(contactDto)) {
            model.addAttribute("result", 1);
        } else {
            model.addAttribute("result", 0);
        }
        return "redirect:/contacts/showcontacts";
    }

    @GetMapping("/showcontacts")
    public ModelAndView showContact() {
        ModelAndView modelAndView = new ModelAndView(ViewConstant.CONTACTS_VIEW);
        modelAndView.addObject("contacts", contactService.listAllContact());
        return modelAndView;
    }

    @GetMapping("/removecontact")
    public ModelAndView removeContact(@RequestParam(name = "id", required = true) int id) {
        contactService.removeContact(id);
        return showContact();
    }

}
