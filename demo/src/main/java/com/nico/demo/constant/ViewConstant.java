package com.nico.demo.constant;

public class ViewConstant {
    public static final String CONTACT_FORM_VIEW = "contactform";
    public static final String LOGIN_VIEW = "login";
    public static final String CONTACTS_VIEW = "contacts";
}
