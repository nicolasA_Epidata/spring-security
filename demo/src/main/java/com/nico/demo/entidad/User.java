package com.nico.demo.entidad;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table
public class User {

    @Id
    @Column(name="userName",unique = true,nullable = false,length = 45)
    private String userName;
    @Column(name="password",nullable = false,length = 60)
    private String password;
    @Column(name="enabled",nullable = false)
    private boolean enabled;
    @OneToMany(fetch = FetchType.EAGER,mappedBy = "user")
    private Set<Role> role = new HashSet<Role>();

    public User(String userName, String password, boolean enabled, Set<Role> role) {
        super();
        this.userName = userName;
        this.password = password;
        this.enabled = enabled;
        this.role = role;
    }

    public User(String userName, String password, boolean enabled) {
        super();
        this.userName = userName;
        this.password = password;
        this.enabled = enabled;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Set<Role> getRole() {
        return role;
    }

    public void setRole(Set<Role> role) {
        this.role = role;
    }

    public User(){}
}
