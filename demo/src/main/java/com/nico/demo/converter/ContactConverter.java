package com.nico.demo.converter;

import com.nico.demo.dto.ContactDto;
import com.nico.demo.entidad.Contact;
import org.springframework.stereotype.Component;

@Component("contactConverter")
public class ContactConverter {

    //ENTITY ---> MODEL
    public ContactDto entity2model(Contact contact) {

        ContactDto contactDto = new ContactDto();
        contactDto.setId(contact.getId());
        contactDto.setFirstName(contact.getFirstName());
        contactDto.setLastName(contact.getLastName());
        contactDto.setTelephone(contact.getTelephone());
        contactDto.setCity(contact.getCity());
        return contactDto;
    }

    //MODEL ___>ENTITY
    public Contact model2entity(ContactDto contactDto) {
        Contact contact = new Contact();
        contact.setId(contactDto.getId());
        contact.setFirstName(contactDto.getFirstName());
        contact.setLastName(contactDto.getLastName());
        contact.setTelephone(contactDto.getTelephone());
        contact.setCity(contactDto.getCity());
        return contact;
    }
}
